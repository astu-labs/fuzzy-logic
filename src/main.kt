import com.fuzzylite.Engine
import com.fuzzylite.Op
import com.fuzzylite.activation.General
import com.fuzzylite.defuzzifier.Centroid
import com.fuzzylite.imex.FisImporter
import com.fuzzylite.norm.s.Maximum
import com.fuzzylite.norm.t.AlgebraicProduct
import com.fuzzylite.norm.t.Minimum
import com.fuzzylite.rule.Rule
import com.fuzzylite.rule.RuleBlock
import com.fuzzylite.term.Trapezoid
import com.fuzzylite.variable.InputVariable
import com.fuzzylite.variable.OutputVariable
import java.io.File

const val CLAPAN_NAME = "clapan"
const val CLAPAN_TERM_1 = "Закрыть_клапан"
const val CLAPAN_TERM_2 = "Открыть_клапан_чуть-чуть"
const val CLAPAN_TERM_3 = "Открыть_клапан_немного"
const val CLAPAN_TERM_4 = "Открыть_клапан_не_очень_сильно"
const val CLAPAN_TERM_5 = "Открыть_клапан_сильно"

const val TRAIN_SPEED_NAME = "speed"
const val TRAIN_SPEED_TERM_1 = "Стоит_на_месте"
const val TRAIN_SPEED_TERM_2 = "Очень_медленно"
const val TRAIN_SPEED_TERM_3 = "Медленно"
const val TRAIN_SPEED_TERM_4 = "Быстро"
const val TRAIN_SPEED_TERM_5 = "Очень_быстро"

const val ROTATE_SPEED_NAME = "rotate_speed"
const val ROTATE_SPEED_TERM_1 = "Не_вращается"
const val ROTATE_SPEED_TERM_2 = "Очень_медленная"
const val ROTATE_SPEED_TERM_3 = "Медленная"
const val ROTATE_SPEED_TERM_4 = "Средняя"
const val ROTATE_SPEED_TERM_5 = "Быстрая"
const val ROTATE_SPEED_TERM_6 = "Очень_быстрая"


fun main() {
    val engine = getEngine()
    val status = StringBuilder()
    if (!engine.isReady(status)) {
        println("engine is not ready: $status")
        return
    }

    val speed = engine.getInputVariable(TRAIN_SPEED_NAME)
    val rotateSpeed = engine.getInputVariable(ROTATE_SPEED_NAME)
    val clapan = engine.getOutputVariable(CLAPAN_NAME)
    speed.value = 50.0
    rotateSpeed.value = 1000.0

    for (i in 0..60 step 10) {
        for (j in 0..2000 step 100) {
            speed.value = i.toDouble()
            rotateSpeed.value = j.toDouble()
            engine.process()
            println("speed.input = ${speed.value}, rotateSpeed.value = ${rotateSpeed.value} -> clapan.output = ${clapan.value}")
        }
    }

}

fun getEngine(): Engine {
    val engine = Engine()
    engine.name = "Steam train"

    InputVariable().apply {
        name = TRAIN_SPEED_NAME
        isEnabled = true
        setRange(0.0, 60.0)
        isLockValueInRange = true
        addTerm(Trapezoid(TRAIN_SPEED_TERM_1, -1.0, 0.0, 1.0, 2.0))
        addTerm(Trapezoid(TRAIN_SPEED_TERM_2, 1.0, 3.0, 10.0, 15.0))
        addTerm(Trapezoid(TRAIN_SPEED_TERM_3, 10.0, 15.0, 25.0, 30.0))
        addTerm(Trapezoid(TRAIN_SPEED_TERM_4, 28.0, 35.0, 45.0, 50.0))
        addTerm(Trapezoid(TRAIN_SPEED_TERM_5, 45.0, 55.0, 60.0, 70.0))
        engine.addInputVariable(this)
    }


    InputVariable().apply {
        name = ROTATE_SPEED_NAME
        isEnabled = true
        setRange(0.0, 2000.0)
        isLockValueInRange = true
        addTerm(Trapezoid(ROTATE_SPEED_TERM_1, -1.0, 0.0, 10.0, 20.0))
        addTerm(Trapezoid(ROTATE_SPEED_TERM_2, 15.0, 30.0, 100.0, 150.0))
        addTerm(Trapezoid(ROTATE_SPEED_TERM_3, 120.0, 180.0, 650.0, 700.0))
        addTerm(Trapezoid(ROTATE_SPEED_TERM_4, 690.0, 750.0, 1200.0, 1300.0))
        addTerm(Trapezoid(ROTATE_SPEED_TERM_5, 1200.0, 1300.0, 1600.0, 1700.0))
        addTerm(Trapezoid(ROTATE_SPEED_TERM_6, 1600.0, 1700.0, 2000.0, 2001.0))
        engine.addInputVariable(this)
    }

    OutputVariable().apply {
        name = CLAPAN_NAME
        isEnabled = true
        setRange(0.0, 1.0)
        isLockValueInRange = false
        aggregation = Maximum()
        defuzzifier = Centroid(100)
        defaultValue = 0.0
        isLockPreviousValue = false
        addTerm(Trapezoid(CLAPAN_TERM_1, -1.0, 0.0, 0.1, 0.15))
        addTerm(Trapezoid(CLAPAN_TERM_2, 0.10, 0.15, 0.20, 0.25))
        addTerm(Trapezoid(CLAPAN_TERM_3, 0.22, 0.30, 0.50, 0.55))
        addTerm(Trapezoid(CLAPAN_TERM_4, 0.45, 0.55, 0.70, 0.80))
        addTerm(Trapezoid(CLAPAN_TERM_5, 0.75, 0.85, 1.0, 1.1))
        engine.addOutputVariable(this)
    }

    RuleBlock().apply {
        name = "mamdani"
        isEnabled = true
        conjunction = Minimum()
        disjunction = Maximum()
        implication = Minimum()
        activation = General()
        addRule(Rule.parse("if $TRAIN_SPEED_NAME is $TRAIN_SPEED_TERM_1 and $ROTATE_SPEED_NAME is $ROTATE_SPEED_TERM_1 then $CLAPAN_NAME is $CLAPAN_TERM_1", engine))
        addRule(Rule.parse("if $TRAIN_SPEED_NAME is $TRAIN_SPEED_TERM_1 and $ROTATE_SPEED_NAME is $ROTATE_SPEED_TERM_2 then $CLAPAN_NAME is $CLAPAN_TERM_1", engine))
        addRule(Rule.parse("if $TRAIN_SPEED_NAME is $TRAIN_SPEED_TERM_1 and $ROTATE_SPEED_NAME is $ROTATE_SPEED_TERM_3 then $CLAPAN_NAME is $CLAPAN_TERM_2", engine))
        addRule(Rule.parse("if $TRAIN_SPEED_NAME is $TRAIN_SPEED_TERM_1 and $ROTATE_SPEED_NAME is $ROTATE_SPEED_TERM_4 then $CLAPAN_NAME is $CLAPAN_TERM_2", engine))
        addRule(Rule.parse("if $TRAIN_SPEED_NAME is $TRAIN_SPEED_TERM_1 and $ROTATE_SPEED_NAME is $ROTATE_SPEED_TERM_5 then $CLAPAN_NAME is $CLAPAN_TERM_2", engine))
        addRule(Rule.parse("if $TRAIN_SPEED_NAME is $TRAIN_SPEED_TERM_1 and $ROTATE_SPEED_NAME is $ROTATE_SPEED_TERM_6 then $CLAPAN_NAME is $CLAPAN_TERM_3", engine))

        addRule(Rule.parse("if $TRAIN_SPEED_NAME is $TRAIN_SPEED_TERM_2 and $ROTATE_SPEED_NAME is $ROTATE_SPEED_TERM_1 then $CLAPAN_NAME is $CLAPAN_TERM_2", engine))
        addRule(Rule.parse("if $TRAIN_SPEED_NAME is $TRAIN_SPEED_TERM_2 and $ROTATE_SPEED_NAME is $ROTATE_SPEED_TERM_2 then $CLAPAN_NAME is $CLAPAN_TERM_2", engine))
        addRule(Rule.parse("if $TRAIN_SPEED_NAME is $TRAIN_SPEED_TERM_2 and $ROTATE_SPEED_NAME is $ROTATE_SPEED_TERM_3 then $CLAPAN_NAME is $CLAPAN_TERM_3", engine))
        addRule(Rule.parse("if $TRAIN_SPEED_NAME is $TRAIN_SPEED_TERM_2 and $ROTATE_SPEED_NAME is $ROTATE_SPEED_TERM_4 then $CLAPAN_NAME is $CLAPAN_TERM_3", engine))
        addRule(Rule.parse("if $TRAIN_SPEED_NAME is $TRAIN_SPEED_TERM_2 and $ROTATE_SPEED_NAME is $ROTATE_SPEED_TERM_5 then $CLAPAN_NAME is $CLAPAN_TERM_3", engine))
        addRule(Rule.parse("if $TRAIN_SPEED_NAME is $TRAIN_SPEED_TERM_2 and $ROTATE_SPEED_NAME is $ROTATE_SPEED_TERM_6 then $CLAPAN_NAME is $CLAPAN_TERM_4", engine))

        addRule(Rule.parse("if $TRAIN_SPEED_NAME is $TRAIN_SPEED_TERM_3 and $ROTATE_SPEED_NAME is $ROTATE_SPEED_TERM_1 then $CLAPAN_NAME is $CLAPAN_TERM_3", engine))
        addRule(Rule.parse("if $TRAIN_SPEED_NAME is $TRAIN_SPEED_TERM_3 and $ROTATE_SPEED_NAME is $ROTATE_SPEED_TERM_2 then $CLAPAN_NAME is $CLAPAN_TERM_3", engine))
        addRule(Rule.parse("if $TRAIN_SPEED_NAME is $TRAIN_SPEED_TERM_3 and $ROTATE_SPEED_NAME is $ROTATE_SPEED_TERM_3 then $CLAPAN_NAME is $CLAPAN_TERM_4", engine))
        addRule(Rule.parse("if $TRAIN_SPEED_NAME is $TRAIN_SPEED_TERM_3 and $ROTATE_SPEED_NAME is $ROTATE_SPEED_TERM_4 then $CLAPAN_NAME is $CLAPAN_TERM_4", engine))
        addRule(Rule.parse("if $TRAIN_SPEED_NAME is $TRAIN_SPEED_TERM_3 and $ROTATE_SPEED_NAME is $ROTATE_SPEED_TERM_5 then $CLAPAN_NAME is $CLAPAN_TERM_5", engine))
        addRule(Rule.parse("if $TRAIN_SPEED_NAME is $TRAIN_SPEED_TERM_3 and $ROTATE_SPEED_NAME is $ROTATE_SPEED_TERM_6 then $CLAPAN_NAME is $CLAPAN_TERM_5", engine))

        addRule(Rule.parse("if $TRAIN_SPEED_NAME is $TRAIN_SPEED_TERM_4 and $ROTATE_SPEED_NAME is $ROTATE_SPEED_TERM_1 then $CLAPAN_NAME is $CLAPAN_TERM_4", engine))
        addRule(Rule.parse("if $TRAIN_SPEED_NAME is $TRAIN_SPEED_TERM_4 and $ROTATE_SPEED_NAME is $ROTATE_SPEED_TERM_2 then $CLAPAN_NAME is $CLAPAN_TERM_4", engine))
        addRule(Rule.parse("if $TRAIN_SPEED_NAME is $TRAIN_SPEED_TERM_4 and $ROTATE_SPEED_NAME is $ROTATE_SPEED_TERM_3 then $CLAPAN_NAME is $CLAPAN_TERM_4", engine))
        addRule(Rule.parse("if $TRAIN_SPEED_NAME is $TRAIN_SPEED_TERM_4 and $ROTATE_SPEED_NAME is $ROTATE_SPEED_TERM_4 then $CLAPAN_NAME is $CLAPAN_TERM_4", engine))
        addRule(Rule.parse("if $TRAIN_SPEED_NAME is $TRAIN_SPEED_TERM_4 and $ROTATE_SPEED_NAME is $ROTATE_SPEED_TERM_5 then $CLAPAN_NAME is $CLAPAN_TERM_5", engine))
        addRule(Rule.parse("if $TRAIN_SPEED_NAME is $TRAIN_SPEED_TERM_4 and $ROTATE_SPEED_NAME is $ROTATE_SPEED_TERM_6 then $CLAPAN_NAME is $CLAPAN_TERM_5", engine))

        addRule(Rule.parse("if $TRAIN_SPEED_NAME is $TRAIN_SPEED_TERM_5 and $ROTATE_SPEED_NAME is $ROTATE_SPEED_TERM_1 then $CLAPAN_NAME is $CLAPAN_TERM_4", engine))
        addRule(Rule.parse("if $TRAIN_SPEED_NAME is $TRAIN_SPEED_TERM_5 and $ROTATE_SPEED_NAME is $ROTATE_SPEED_TERM_2 then $CLAPAN_NAME is $CLAPAN_TERM_4", engine))
        addRule(Rule.parse("if $TRAIN_SPEED_NAME is $TRAIN_SPEED_TERM_5 and $ROTATE_SPEED_NAME is $ROTATE_SPEED_TERM_3 then $CLAPAN_NAME is $CLAPAN_TERM_5", engine))
        addRule(Rule.parse("if $TRAIN_SPEED_NAME is $TRAIN_SPEED_TERM_5 and $ROTATE_SPEED_NAME is $ROTATE_SPEED_TERM_4 then $CLAPAN_NAME is $CLAPAN_TERM_5", engine))
        addRule(Rule.parse("if $TRAIN_SPEED_NAME is $TRAIN_SPEED_TERM_5 and $ROTATE_SPEED_NAME is $ROTATE_SPEED_TERM_5 then $CLAPAN_NAME is $CLAPAN_TERM_5", engine))
        addRule(Rule.parse("if $TRAIN_SPEED_NAME is $TRAIN_SPEED_TERM_5 and $ROTATE_SPEED_NAME is $ROTATE_SPEED_TERM_6 then $CLAPAN_NAME is $CLAPAN_TERM_5", engine))
        engine.addRuleBlock(this)
    }

    return engine
}